unit XPlatformTests;

interface

uses
   Classes, SysUtils, OmniXML,
   {$ifdef FPC}
   fpcunit, testutils, testregistry
   {$else}
   TestFrameWork
   {$endif}
   ;

type

   {$ifdef FPC}

   TBaseTestCase = class (fpcunit.TTestCase)
      public
         procedure CheckEquals(const expected, actual: UnicodeString; const msg: String = ''); overload;
         procedure CheckEquals(const expected : String; const actual: UnicodeString; const msg: String = ''); overload;
   end;

   ETestFailure = class (Exception);

   {$else}

   TBaseTestCase = TestFrameWork.TTestCase;

   ETestFailure = TestFrameWork.ETestFailure;

   {$endif}

   TTestCase = class (TBaseTestCase)
      protected
         procedure assert_xpath(const msg, xpath, xml : String);
         function  render_string(const adoc : String) : String;

         function  XPathText(const xpath, xml : String) : String;
   end;

procedure RegisterTest(const testName : String; aTest : TTestCaseClass);

// ------------------------------------------------------------------
// ------------------------------------------------------------------
// ------------------------------------------------------------------
implementation
// ------------------------------------------------------------------
// ------------------------------------------------------------------
// ------------------------------------------------------------------

// RegisterTest
//
procedure RegisterTest(const testName : String; aTest : TTestCaseClass);
begin
   {$ifdef FPC}
   testregistry.RegisterTest(aTest);
   {$else}
   TestFrameWork.RegisterTest(testName, aTest.Suite);
   {$endif}
end;

// CheckEquals
//
{$ifdef FPC}
procedure TTestCase.CheckEquals(const expected, actual: UnicodeString; const msg: String = '');
begin
   AssertTrue(msg + ComparisonMsg(Expected, Actual), AnsiCompareStr(Expected, Actual) = 0);
end;
procedure TTestCase.CheckEquals(const expected : String; const actual: UnicodeString; const msg: String = '');
begin
   AssertTrue(msg + ComparisonMsg(Expected, Actual), AnsiCompareStr(Expected, Actual) = 0);
end;
{$endif}

// assert_xpath
//
procedure TTestCase.assert_xpath(const msg, xpath, xml : String);
var
   doc : IXMLDocument;
   nodeList : IXMLNodeList;
begin
   doc:=TXMLDocument.Create;
   try
      doc.LoadXML(xml);
   except
      on E: Exception do begin
         Fail(msg+#13#10+E.Message);
         exit;
      end;
   end;

   nodeList:=doc.SelectNodes(xpath);
   Check(nodeList.Length>0, msg);
end;

// XPathText
//
function TTestCase.XPathText(const xpath, xml : String) : String;
var
   doc : IXMLDocument;
   nodeList : IXMLNodeList;
   i : Integer;
begin
   doc:=TXMLDocument.Create;
   try
      doc.LoadXML(xml);
      nodeList:=doc.SelectNodes(xpath);
   except
      on E: Exception do begin
         Result:=E.ClassName+': '+E.Message;
         exit;
      end;
   end;
   Result:='';
   for i:=0 to nodeList.Length-1 do
      Result:=Result+nodeList.Item[i].Text;
end;

// render_string
//
function TTestCase.render_string(const adoc : String) : String;
begin
   // TODO!!!
   Result:=adoc;
end;

end.


unit TestXPlatformTests;

interface

uses
   Classes, SysUtils, XPlatformTests;

type

   TTestXPlatformTests = class (TTestCase)
      private

      public

      published
         procedure AssertXPathTest;
         procedure CheckXPathText;

   end;

// ------------------------------------------------------------------
// ------------------------------------------------------------------
// ------------------------------------------------------------------
implementation
// ------------------------------------------------------------------
// ------------------------------------------------------------------
// ------------------------------------------------------------------


// ------------------
// ------------------ TTestXPlatformTests ------------------
// ------------------

// AssertXPathTest
//
procedure TTestXPlatformTests.AssertXPathTest;
begin
   assert_xpath('Find element', 'p', '<p>hello</p>');
   assert_xpath('Find attribute', '//a[@a=''b'']', '<a a="b" />');
end;

// CheckXPathText
//
procedure TTestXPlatformTests.CheckXPathText;
begin
   CheckEquals('hello', XPathText('//a', '<a>hello</a>'), 'hello');
   CheckEquals('hello', XPathText('//a[@r="/e"]', '<a>hello</a><a r="/e">bye</a>'), 'bye');
end;

// ------------------------------------------------------------------
// ------------------------------------------------------------------
// ------------------------------------------------------------------
initialization
// ------------------------------------------------------------------
// ------------------------------------------------------------------
// ------------------------------------------------------------------

   RegisterTest('XPlatformTests', TTestXPlatformTests);

end.


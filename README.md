# ASCIIdoc for Pascal #

This projects aims at eventually creating a Pascal version of ASCIIdoc processing.

Primary goals are parsing ASCIIdoc, generating a DOM, and rendering to HTML, in a manner compliant with the ASCIIdoc test suite.

Secondary goal is [DWScript](https://bitbucket.org/egrange/dwscript) compatibility and compilation to JS.